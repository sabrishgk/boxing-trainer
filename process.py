from typing_extensions import get_args
import gtts
from playsound import playsound
import argparse
from json import (
    load as jload
)
from tqdm import tqdm
from random import choice
from time import sleep
from os import makedirs


def get_args():
    """
    parse in args
    """

    parser = argparse.ArgumentParser(description='Coach guide')

    parser.add_argument(
        '-r', '--rounds', type=int, help='How many rounds to you want to do?', required=True)

    parser.add_argument('-i', '--iterations', type=int,
                        help='Iterations per round', required=True)

    parser.add_argument(
        '-p', '--routine', help='Which routine do you want to load?', required=False)

    return parser.parse_args()


if __name__ == "__main__":

    # parse in args
    args = get_args()

    # indexable seq
    taskList = []

    # make bites directory
    makedirs("bites", exist_ok=True)

    # load routine and gen soundbites
    if not args.routine:
        args.routine = "day2"

    with open(f"./routine/{args.routine}.json", "r") as file:
        routine = jload(file)

    print("preparing soundbytes...")

    print("registering moves...")

    for task in tqdm(routine.keys()):
        # make request to google to get synthesis
        tts = gtts.gTTS(routine[task]["text"])

        # save the audio file
        tts.save(f"./bites/{task}.mp3")

        # append to taskList
        if "complete" not in task:
            taskList.append(task)

    print("registering rounds...")

    for round in tqdm(range(args.rounds)):
        # make request to google to get synthesis
        tts = gtts.gTTS(f"round {round+1}")

        # save the audio file
        tts.save(f"./bites/r{round}.mp3")

    print("done...")

    print("Get ready...")

    for _ in tqdm(range(5)):
        sleep(1)

    for round in tqdm(range(args.rounds)):

        # play the audio file
        playsound(
            f"./bites/r{round}.mp3")

        for iteration in tqdm(range(args.iterations)):
            random_task = choice(taskList)

            # play the audio file
            playsound(
                f"./bites/{random_task}.mp3")

            # wait
            sleep(routine[random_task]["time"])
        else:

            playsound("./bites/rcomplete.mp3")
            sleep(routine["rcomplete"]["time"])
    else:

        playsound("./bites/wcomplete.mp3")
        sleep(routine["wcomplete"]["time"])
